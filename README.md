# Geodis Image-deploy content pack


Introduction :

1. This is Geodis Image-deploy content Params,Profiles,Tasks,Stages,Workflows related server deployment, Please read the following to use this repo. 

 All files should be in the "/content" directory , all files are managed in Yaml file format and should use "-f yaml" for all drpcli commands to get output in Yaml format. 


Usage:


To download the copy of geodis-image-deploy-content pack to your working directory
 
      # git clone https://gitlab.com/rgandla/rackn-content-pack.git
      # cd ./rackn-content-pack/content
   
Make all changes to the coresponding yaml files in Params,Profiles,Tasks,Stages,Workflows etc . created a bundle out of the files in the content directory which output a Yaml file. 
 
     # drpcli contents bundle  ../geodis-image-deploy-content.yaml

Upload the contents drp endpoint 
    #drpcli contents create  --replaceWritable /tmp/geodis-image-deploy.yaml    [ --replaceWriteable flag if you have same named objects in DRP endpoint]  
    #drpcli  contents update geodis-image-deploy-content   ../geodis-image-deploy-content.yaml   

Since we have the Secure content [rancher token etc ], there are some additional steps. 

Download the same Content Pack bundle from the DRP server and save the encryption key: drpcli contents show [id] –key [file] > [secure content file].

    #drpcli contents show geodis-image-deploy-content  --key  /tmp/keyfile.json  > /tmp/secure-geodis-image-deploy.yaml   [ keyfile.json is the key file to be used to show, update create contenent pack]

    # ls -l /tmp/keyfile.json 

Remove the original content pack 
     #drpcli contents destroy  geodis-image-deploy-content  

Create and Upload the content pack   

     # drpcli contents create ../secure-geodis-image-deploy-content.yaml   --key  /tmp/keyfile.json 




